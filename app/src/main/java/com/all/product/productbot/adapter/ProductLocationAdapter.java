package com.all.product.productbot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.all.product.productbot.R;
import com.all.product.productbot.model.modelproductlocation;
import com.all.product.productbot.model.modelsizechart;

import java.util.ArrayList;

/**
 * Created by 599584 on 4/11/2017.
 */

public class ProductLocationAdapter extends RecyclerView.Adapter<ProductLocationAdapter.ProductlocationViewHolder> {
    private ArrayList<modelproductlocation> modelproductlocationArrayList;
    private Context mContext;

    public ProductLocationAdapter(Context context, ArrayList<modelproductlocation> modelproductlocationArrayList){
        this.modelproductlocationArrayList=modelproductlocationArrayList;
        this.mContext=context;
    }
    @Override
    public ProductLocationAdapter.ProductlocationViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.productlocation, viewGroup, false);
        ProductLocationAdapter.ProductlocationViewHolder productlocationViewHolder=new ProductLocationAdapter.ProductlocationViewHolder(v);
        return productlocationViewHolder;
    }
    @Override
    public void onBindViewHolder(ProductLocationAdapter.ProductlocationViewHolder holder, final int i) {

        modelproductlocation model= modelproductlocationArrayList.get(i);
        holder.pic.setImageResource(model.getPic());
    }


    public class ProductlocationViewHolder extends RecyclerView.ViewHolder {

        public ImageView pic;

        public ProductlocationViewHolder(View view) {
            super(view);
            pic=(ImageView) view.findViewById(R.id.chartImage);
        }
    }







    @Override
    public int getItemCount() {
        return modelproductlocationArrayList.size();
    }
}
