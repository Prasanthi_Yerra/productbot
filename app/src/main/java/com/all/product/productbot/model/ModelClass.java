package com.all.product.productbot.model;

import java.util.List;

public class ModelClass {
    private double confidence;
    private String type;
    private String action;
    private EntitiesEntity entities;
    private String msg;
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setEntities(EntitiesEntity entities) {
        this.entities = entities;
    }

    public double getConfidence() {
        return confidence;
    }

    public String getType() {
        return type;
    }

    public String getAction() {
        return action;
    }

    public EntitiesEntity getEntities() {
        return entities;
    }

    public static class EntitiesEntity {
        /**
         * productName : [{"confidence":0.9637129033346896,"type":"value","value":"Croco Drawer"}]
         * intent : [{"confidence":0.9950420810328074,"value":"productLocation"}]
         */

        private List<ProductNameEntity> productName;
        private List<IntentEntity> intent;

        public void setProductName(List<ProductNameEntity> productName) {
            this.productName = productName;
        }

        public void setIntent(List<IntentEntity> intent) {
            this.intent = intent;
        }

        public List<ProductNameEntity> getProductName() {
            return productName;
        }

        public List<IntentEntity> getIntent() {
            return intent;
        }

        public static class ProductNameEntity {
            /**
             * confidence : 0.9637129033346896
             * type : value
             * value : Croco Drawer
             */

            private double confidence;
            private String type;
            private String value;

            public void setConfidence(double confidence) {
                this.confidence = confidence;
            }

            public void setType(String type) {
                this.type = type;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public double getConfidence() {
                return confidence;
            }

            public String getType() {
                return type;
            }

            public String getValue() {
                return value;
            }
        }

        public static class IntentEntity {
            /**
             * confidence : 0.9950420810328074
             * value : productLocation
             */

            private double confidence;
            private String value;

            public void setConfidence(double confidence) {
                this.confidence = confidence;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public double getConfidence() {
                return confidence;
            }

            public String getValue() {
                return value;
            }
        }
    }


//    private double confidence;
//    private String type;
//    private String action;
//    private String msg;
//
//    private List<String> quickreplies;
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    private EntitiesBean entities;
//
//    public double getConfidence() {
//        return confidence;
//    }
//
//    public void setConfidence(double confidence) {
//        this.confidence = confidence;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public String getAction() {
//        return action;
//    }
//
//    public void setAction(String action) {
//        this.action = action;
//    }
//
//    public EntitiesBean getEntities() {
//        return entities;
//    }
//    public void setQuickreplies(List<String> quickreplies){
//        this.quickreplies = quickreplies;
//    }
//    public List<String> getQuickreplies(){
//        return this.quickreplies;
//    }
//
//    public void setEntities(EntitiesBean entities) {
//        this.entities = entities;
//    }
//
//    public static class EntitiesBean {
//        private List<PersonBean> person;
//        private List<IntentBean> intent;
//        private List<productNameBean> productName;
//
//        public List<PersonBean> getPerson() {
//            return person;
//        }
//
//        public void setPerson(List<PersonBean> person) {
//            this.person = person;
//        }
//        public List<productNameBean> getproductName() {
//            return productName;
//        }
//
//        public void setproductName(List<productNameBean> productName) {
//            this.productName = productName;}
//
//
//        public List<IntentBean> getIntent() {
//            return intent;
//        }
//
//        public void setIntent(List<IntentBean> intent) {
//            this.intent = intent;
//        }
//        public static class productNameBean {
//            /**
//             * confidence : 0.8325042685323366
//             * type : value
//             * value : soho
//             * suggested : true
//             */
//
//            private double confidence;
//            private String type;
//            private String value;
//
//
//            public double getConfidence() {
//                return confidence;
//            }
//
//            public void setConfidence(double confidence) {
//                this.confidence = confidence;
//            }
//
//            public String getType() {
//                return type;
//            }
//
//            public void setType(String type) {
//                this.type = type;
//            }
//
//            public String getValue() {
//                return value;
//            }
//
//            public void setValue(String value) {
//                this.value = value;
//            }
//
//
//        }
//        public static class PersonBean {
//            /**
//             * confidence : 0.8325042685323366
//             * type : value
//             * value : soho
//             * suggested : true
//             */
//
//            private double confidence;
//            private String type;
//            private String value;
//            private boolean suggested;
//
//            public double getConfidence() {
//                return confidence;
//            }
//
//            public void setConfidence(double confidence) {
//                this.confidence = confidence;
//            }
//
//            public String getType() {
//                return type;
//            }
//
//            public void setType(String type) {
//                this.type = type;
//            }
//
//            public String getValue() {
//                return value;
//            }
//
//            public void setValue(String value) {
//                this.value = value;
//            }
//
//            public boolean isSuggested() {
//                return suggested;
//            }
//
//            public void setSuggested(boolean suggested) {
//                this.suggested = suggested;
//            }
//        }
//
//        public static class IntentBean {
//            /**
//             * confidence : 0.9882574840960712
//             * value : greetings
//             */
//
//            private double confidence;
//            private String value;
//
//            public double getConfidence() {
//                return confidence;
//            }
//
//            public void setConfidence(double confidence) {
//                this.confidence = confidence;
//            }
//
//            public String getValue() {
//                return value;
//            }
//
//            public void setValue(String value) {
//                this.value = value;
//            }
//        }
//    }
}
