package com.all.product.productbot.module;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.all.product.productbot.R;

public class RequestAssociatePopUp extends AppCompatActivity {

    String productName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_associate_pop_up);

        productName = getIntent().getStringExtra("name");

        TextView textView = (TextView) findViewById(R.id.product_name);
        textView.setText(productName);
    }

    public void finishActivity(View view) {
        finish();
    }

    public void showAsociateDetails(View view) {
        startActivity(new Intent(RequestAssociatePopUp.this, AssociateDetailsPopUp.class));
        finish();
    }
}
