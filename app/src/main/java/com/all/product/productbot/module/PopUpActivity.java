package com.all.product.productbot.module;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.all.product.productbot.R;

public class PopUpActivity extends AppCompatActivity {

    String productName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pop_up);

        productName = getIntent().getStringExtra("name");
        TextView textView = (TextView) findViewById(R.id.product_name);
        textView.setText("on " + productName);
    }

    public void closeActivity(View view) {
        finish();
    }

    public void showVideo(View view) {
        startActivityForResult(new Intent(PopUpActivity.this, VideoActivity.class), 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2) {
            Log.e("@@##", "#####");

            startActivity(new Intent(PopUpActivity.this, RequestAssociatePopUp.class).putExtra("name", productName));
            finish();
        }

    }
}
