package com.all.product.productbot.module;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

import com.all.product.productbot.R;

import static com.all.product.productbot.R.id.videoView;

public class VideoActivity extends AppCompatActivity {
    VideoView view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video);

        view = (VideoView) findViewById(videoView);

        playVideo();

        view.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.e("@@##", "yay");
                Intent intent = new Intent();
                setResult(2, intent);
                finish();

            }
        });


    }

    public void playVideo() {
        MediaController mc = new MediaController(this);
        view.setMediaController(mc);
        String path = "android.resource://" + getPackageName() + "/" + R.raw.movie_video;
        view.setVideoURI(Uri.parse(path));
        view.start();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(2, intent);
        finish();
        super.onBackPressed();
    }
}
