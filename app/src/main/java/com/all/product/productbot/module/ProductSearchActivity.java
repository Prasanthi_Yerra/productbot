package com.all.product.productbot.module;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.all.product.productbot.*;
import com.all.product.productbot.MainActivity;
import com.all.product.productbot.module.models.ProductResponse;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class ProductSearchActivity extends AppCompatActivity {

    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_search);

        gson = new Gson();

        final ProductResponse[] productResponses = gson.fromJson(loadJSONFromAsset(), ProductResponse[].class);

        final String[] productList = new String[productResponses.length];

        for (int i = 0; i < productResponses.length; i++) {
            productList[i] = productResponses[i].getProductName();
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, productList);
        AutoCompleteTextView actv = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
        actv.setThreshold(1);
        actv.setAdapter(adapter);
        actv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selected = (String) parent.getItemAtPosition(position);
                int pos = Arrays.asList(productList).indexOf(selected);
                startActivity(new Intent(ProductSearchActivity.this, MainActivity.class).putExtra("id", productResponses[pos]));
            }
        });

    }


    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open("response.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
